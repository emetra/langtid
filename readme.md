Om LANGTID
==========

Her finner du dokumenter som er spesifikke for LANGTID-protokollen.
Disse filene brukes p.t. i FastTrak i Hadsel Kommune.
I tillegg til filene som ligger her bruker kunden en rekke filer som kommer fra GBD-protokollen.
Disse må lastes ned med FastTrakUpdate, eventuelt tas ut fra ZIP-filer som lages av byggeserveren.

Mapper
======
 
  * **(rot)** - Inneholder startbilde/dokumentasjon.
  * **Images** - Inneholder bilder som inngår i dokumentasjon.

Forsiden
========

I roten ligger filen [index.html](https://binaries.dips.no/LANGTID/index.html).  Dette er forsiden som viser for brukerne.
Startsiden ligger på [nedlastingsserveren](https://binaries.dips.no) og oppdateres som en del av byggeprosessen.

Installasjon
============
Pakk ut nedlastet zip-fil og kopier inn med overskriving av eksisterende filer. 
Kjør deretter `\bin\FastTrakUpdate.exe` og kryss av for alle tabeller før kjøring.
Husk å krysse av for "Lokale filer" hvis du ikke har internett-tilgang.

Bergen, 15. august 2018

Magne Rekdal,  
Medisinsk Rådgiver